---
layout: content2
title: First steps
date: 2021-03-17 13:09:50 +0300
img: 01_who.jpg
headings: 
    - 
        name: Welcome Day
        id: Welcome
   
---
### Welcome Day {#Welcome}

On your first day at work you will meet the human resources team, who will introduce you to your new place of work and teach you a little bit about the history of the company. 

You will receive all  the necessary material to start working: computer, mouse, backpack, etc. and of course the links to useful information so you can start configuring your devices, get to know the center and more. (They will start activating your accounts and the tools you will need but sometimes this takes a little longer than expected, don't worry about that)

Office Installations:

Kitchen: In the kitchen you have a microwave and a refrigerator to leave and heat what you want. There is also a snack machine and a coffee machine (Free coffee with Selecta Card -ask at 6th floor- )

Rooms: You will be able to use the small rooms freely (without previous reservation)
 For the large rooms, you will have to reserve them. (You can do it from Outlook: create a meeting and in location add the room you want)

Rooms location name:

- CR ES VLC P6 AYORA 6P
- CR ES VLC P6 BENIMACLET 3P
- CR ES VLC P6 CABANYAL 4P
- CR ES VLC P6 CAMPANAR 4P
- CR ES VLC P6 MALILLA 6P
- CR ES VLC P6 PATRAIX 4P
- CR ES VLC P6 MESTALLA 18P
- CR ES VLC P6 NAZARET 4P
- CR ES VLC P6 RUZAFA 8P VDO
- CR ES VLC P9 CRISTAL 9V1
- CR ES VLC P9 CRISTAL 9V2
- CR ES VLC P9 CRISTAL 9V3
- CR ES VLC P9 CRISTAL 9V4
- CR ES VLC P9 CRISTAL 9V5


- Office Card: To get the entrance card to the office, go to floor 6 and contact the reception staff.

- Office Seat Reservation: The application where you should reserve your office space is OfficePass. Remember: If you book a day and then you can't come, remember to cancel the reservation so that another colleague can use it.